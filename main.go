package main

import (
	"html/template"
	"log"
	"net/http"
	"fmt"
)

var tpl *template.Template

func home(res http.ResponseWriter, req *http.Request) {
	fmt.Println(req.URL.Path)
	if req.URL.Path != "/" {
		http.NotFound(res, req)
		return
	}
	tpl.ExecuteTemplate(res, "home.html", nil)
}
func about(res http.ResponseWriter, req *http.Request) {
	fmt.Println(req.URL.Path + ", correct")
	tpl.ExecuteTemplate(res, "about.html", nil)
}
func contact(res http.ResponseWriter, req *http.Request) {
	fmt.Println(req.URL.Path + ", correct")
	tpl.ExecuteTemplate(res, "contact.html", nil)
}

func main() {
	tpl = template.Must(template.ParseGlob("templates/*.html"))
	http.HandleFunc("/", home)
	http.HandleFunc("/about", about)
	http.HandleFunc("/contact", contact)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.Handle("/assets/", http.StripPrefix("/assets", http.FileServer(http.Dir("assets/"))))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
