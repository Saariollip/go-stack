package main

import (
    "fmt"
    "log"
    "net/http"
    "github.com/julienschmidt/httprouter"
)


func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  fmt.Fprint(w, "Welcome!\n")
}

func HelloIndex(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  fmt.Fprintf(w, "hello, %s!\n", ps.ByName("name"))
}

func HelloShow(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

}

func main() {
  // http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
  //   fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
  // })

  router := httprouter.New()
  router.GET("/", Index)
  // router.GET("/hello/", HelloIndex)
  router.GET("/hello/:name", HelloIndex)

  log.Fatal(http.ListenAndServe(":8080", router))

}
