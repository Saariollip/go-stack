var app = new Vue({
  el: '#app',
  data: {
    message: 'Vue magic:'
  },
  methods: {
    play: function (event) {
      const audio = new Audio('../assets/mp3/parinelost.mp3');
      audio.play();
    },
    play2: function (event) {
      const audio = new Audio('../assets/mp3/noniii.mp3');
      audio.play();
    },
    play3: function (event) {
      const audio = new Audio('../assets/mp3/hääh.mp3');
      audio.play();
    }
  },
  delimiters: ["${", "}"]
})
